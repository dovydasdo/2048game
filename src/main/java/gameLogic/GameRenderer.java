package gameLogic;

class GameRenderer {
    private Table table;

    GameRenderer(Table renderTable){
        this.table = renderTable;
    }

    void render(){
        for(int i = 0; i < table.getTable().length; i++){
            for(int j = 0; j < table.getTable().length; j++){
                    System.out.print(table.getTable()[i][j] + " ");
            }
            System.out.println();
        }
    }

}
