package gameLogic;

public class Table {

    private int [][]table = {   {0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0},
                                {0, 0, 0, 0}};
    private RandomNumberGenerator randomNumber = new RandomNumberGenerator(0, table.length -1);

    public Table(){
        setNewNum();
        setNewNum();
    }

    public int[][] getTable(){
        return this.table;
    }

    public boolean isFull(){
        for (int[] ints : table) {
            for (int j = 0; j < table.length; j++) {
                if (ints[j] == 0)
                    return false;
            }
        }
        return true;
    }

    public void setNewNum(){
        while(true){

            int newX = randomNumber.getRandomNumber();
            int newY = randomNumber.getRandomNumber();
            if(setTableNum(newX, newY)){
                return;
            }
        }
    }

    private boolean setTableNum(int newX, int newY){
        if(table[newX][newY] == 0){
            if(randomNumber.isTwo()) {
                table[newX][newY] = 2;
            }
            else
                table[newX][newY] = 4;
            return true;
        }
        return false;
    }

    public void setTable(int[][] table) {
        this.table = table;
    }
}
