package gameLogic;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Table table = new Table();
        GameRules gameRules = new GameRules(table);
        GameRenderer renderer = new GameRenderer(table);

        while(true){

            renderer.render();

            if(table.isFull()){
                System.out.println("You lost");
                System.exit(0);
            }
            if(gameRules.is2048()){
                System.out.println("You won");
                System.exit(0);
            }

            if(table.isFull()){
                System.exit(0);
            }

            int command = ConsoleUserInputReader.readUserInput();

            gameRules.pushByUserCommand(command);

        }

    }

}

