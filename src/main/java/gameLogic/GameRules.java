package gameLogic;

public class GameRules {
    private Table table;
    public GameRules(Table table){
        this.table = table;
    }
    
    private void pushAllTableNumsToRight(int[][] table){
        for (int[] ints : table) {
            pushSingleRowNumsToRight(ints);
        }
    }

    public void pushAllTableNumsToLeft(int[][] table){
        for (int[] ints : table) {
            pushSingleRowNumsToLeft(ints);
        }
    }

    public void addSameAllNumsFromRight(int[][] table){
        for (int[] ints : table) {
            addSameSingleRowNumsFromRight(ints);
        }
    }

    public void addSameAllNumsFromLeft(int[][] table){
        for (int[] ints : table) {
            addSameSingleRowNumsFromLeft(ints);
        }
    }

    private void pushToRight(){
        pushAllTableNumsToRight(table.getTable());
        addSameAllNumsFromRight(table.getTable());
        pushAllTableNumsToRight(table.getTable());
    }

    public void pushToLeft(){
        pushAllTableNumsToLeft(table.getTable());
        addSameAllNumsFromLeft(table.getTable());
        pushAllTableNumsToLeft(table.getTable());
    }

    private void pushDown(){
        int [][]temp = shiftTable(table.getTable());
        pushAllTableNumsToRight(temp);
        addSameAllNumsFromRight(temp);
        pushAllTableNumsToRight(temp);
        temp = shiftTable(temp);
        table.setTable(temp);

    }

    private int[][] shiftTable(int[][] table){
        int[][] shiftedTable = new int [table.length][table.length];
        for(int i = 0; i < table.length; i++){
            for(int j = 0; j < table.length; j++){
                shiftedTable[i][j] = table[j][i];
            }
        }
        return shiftedTable;
    }

    private void pushUp(){
        int [][]temp = shiftTable(table.getTable());
        pushAllTableNumsToLeft(temp);
        addSameAllNumsFromLeft(temp);
        pushAllTableNumsToLeft(temp);
        temp = shiftTable(temp);
        table.setTable(temp);
    }

    private static void pushSingleRowNumsToRight(int[] arr){
        int temp;
        for(int i = 0; i< arr.length - 1; i++){
            for(int j = arr.length - 2; j >= 0 ; j--){
                if(arr[j+1] == 0 && arr[j] != 0 ){
                    temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    private static void pushSingleRowNumsToLeft(int[] arr){
        int temp;
        for(int i = 0; i< arr.length - 1; i++){
            for(int j = 0; j <= arr.length - 2 ; j++){
                if(arr[j+1] != 0 && arr[j] == 0 ){
                    temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    private static void addSameSingleRowNumsFromRight(int[] arr){
        for(int i = arr.length -1; i > 0; i--){
            if(arr[i] == arr[i - 1]){
                arr[i]*= 2;
                arr[i - 1] = 0;
                i--;
                if(i == 0)
                    break;
            }
        }
    }

    private static void addSameSingleRowNumsFromLeft(int[] arr){
        for(int i = 0; i <= arr.length - 2 ; i++){
            if(arr[i] == arr[i + 1]){
                arr[i]*= 2;
                arr[i + 1] = 0;
                i++;
                if(i == arr.length -1)
                    break;
            }
        }
    }

    void pushByUserCommand(int character){

        int [][] compTable;
        compTable = getInts(table.getTable());

            switch (character) {
                case 'q':
                    System.exit(0);
                    break;
                case 'w':
                    pushUp();
                    break;
                case 'a':
                    pushToLeft();
                    break;
                case 's':
                    pushDown();
                    break;
                case 'd':
                    pushToRight();
                    break;
            }


        if (tableChanged(compTable, table.getTable()))
                table.setNewNum();

    }

    boolean is2048(){
        for(int i = 0; i < table.getTable().length; i++){
            for(int j = 0; j < table.getTable().length; j++){
                if(table.getTable()[i][j] == 2048){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean tableChanged(int[][] firstTable, int[][] secondTable){

        for(int i = 0; i < firstTable.length; i++){
            for(int j = 0; j < firstTable.length; j++){
                if(firstTable[i][j] != secondTable[i][j]){
                    return true;
                }
            }
        }
        return false;
    }

    private static int[][] getInts(int[][] table) {
        int [][] copyTable = new int[table.length][table.length];
        for(int i = 0; i < table.length; i++){
            System.arraycopy(table[i], 0, copyTable[i], 0, table.length);
        }
        return copyTable;
    }
}
