package gameLogic;

import java.util.Random;

class RandomNumberGenerator {
    private int min;
    private int max;

    RandomNumberGenerator(int min, int max){
        this.min = min;
        this.max = max;

    }

    int getRandomNumber(){
        return (int)(Math.random() * ((max - min) + 1)) + min;
    }

    boolean isTwo(){
        Random random = new Random();
        return random.nextBoolean();
    }
}
