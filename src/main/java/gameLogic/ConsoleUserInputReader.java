package gameLogic;

import java.io.IOException;

class ConsoleUserInputReader {

    static int readUserInput() throws IOException {
        int character;
        do {
            character = System.in.read();
        }while(character == '\n' || character == '\r');
        return character;
    }

}
