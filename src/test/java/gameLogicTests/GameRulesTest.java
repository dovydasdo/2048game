package gameLogicTests;
import gameLogic.GameRules;
import gameLogic.Table;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;


public class GameRulesTest extends TestCase{
    private Table testTable = new Table();

    @Test
    public void testIsFull(){
        assertFalse(testTable.isFull());

        do{
            testTable.setNewNum();
        }while(!testTable.isFull());

        assertTrue(testTable.isFull());

    }

    @Test
    public void testAddLeft(){
        int [][]result = {  {0, 0, 0, 0},
                            {0, 0, 8, 0},
                            {0, 0, 0, 0},
                            {0, 0, 0, 0}};

        int [][]start = {  {0, 0, 0, 0},
                            {0, 0, 4, 4},
                            {0, 0, 0, 0},
                            {0, 0, 0, 0}};

        GameRules rules = new GameRules(testTable);
        rules.addSameAllNumsFromLeft(start);
        assertTrue(Arrays.deepEquals(result, start));
    }

    @Test
    public void testAddRight(){
        int [][]result = {  {0, 0, 0, 0},
                            {0, 0, 0, 16},
                            {0, 0, 0, 0},
                            {0, 0, 0, 0}};

        int [][]start = {   {0, 0, 0, 0},
                            {0, 0, 8, 8},
                            {0, 0, 0, 0},
                            {0, 0, 0, 0}};

        GameRules rules = new GameRules(testTable);
        rules.addSameAllNumsFromRight(start);
        assertTrue(Arrays.deepEquals(result, start));
    }

    @Test
    public void testPushLeft(){
        int [][]result = {  {0, 0, 0, 0},
                            {4, 4, 0, 0},
                            {0, 0, 0, 0},
                            {0, 0, 0, 0}};

        int [][]start = {   {0, 0, 0, 0},
                            {0, 0, 4, 4},
                            {0, 0, 0, 0},
                            {0, 0, 0, 0}};

        GameRules rules = new GameRules(testTable);
        rules.pushAllTableNumsToLeft(start);
        assertTrue(Arrays.deepEquals(result, start));
    }

}
